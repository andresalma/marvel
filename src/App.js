import './App.scss';
import Search from './components/Search/Search';
import User from './components/User/User';
import React, { useState } from 'react';

function App() {

  const [searc, setSearc] = useState([]);

  return (
    <div className="App">
          <Search  setSearc={setSearc}/>
          {searc.map((avenger)=>(   // ( hace return automatico
            <User name= {avenger.name}  picture= {avenger.thumbnail.path + "." +avenger.thumbnail.extension} key= {avenger.id}/>
          ))}
    </div>
  );
}

export default App;
