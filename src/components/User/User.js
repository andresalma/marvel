import './User.scss';

function User (props){
    console.log(props)
    return (
        <div>
            <div className="User">
                <h1 className="User-name">{props.name}</h1>
                <img className="User-pic" src={props.picture} alt="foto" />
            </div>
        </div> 
    );
}

export default User;
