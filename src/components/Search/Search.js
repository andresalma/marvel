import './Search.scss';
import React, { useEffect, useState } from 'react';

function Search(props){

    const [offset, setOffset] = useState(0);

    function filterApi(event) {
        event.preventDefault();
        //
        fetch(`https://gateway.marvel.com:443/v1/public/characters?limit=10&offset=${offset}&apikey=e0c15083b82ced91270c0101782651f1`)
               
        .then(res => res.json())//la respuesta
        .then( dataApi => {
            console.log("captura del Api es: ")
            console.log(dataApi.data.results)
            props.setSearc(dataApi.data.results)
            setOffset((prev)=>prev+10)//recibe offset
        });
    }

    return (
        <form type="submit" onSubmit={filterApi} className="search__form">
          <input type="image" src="/img/lupa3.png" alt="Lupa" className="search__form__lupa" />
        </form>
    );
}

export default Search;